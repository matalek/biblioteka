/** Projekt biblioteka
 * Aleksander Matusiak
 * 
 * Klasa do obsługi administracji książkami, autorami i egzemplarzami
 */

package biblioteka;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author aleksander
 */
public class AdministracjaKsiążkami {

    public AdministracjaKsiążkami(Connection c, Scanner sc, Statement stmt, ResultSet rs) {
        this.c = c;
        this.sc = sc;
        this.stmt = stmt;
        this.rs = rs;
    }
    
    public void obsługa() {
        boolean guard = true;
        System.out.println("Panel administracji książkami.");
        System.out.println("Wpisz h, aby uzyskać pomoc. Wpisz a w dowolnym "
                + "momencie, aby anulować aktualnie wykonywaną czynność.");
        while (guard) {
            switch (sc.nextLine()) {
                case "h":
                    wyświetlMenu();
                    break;
                case "da":
                    dodajAutora();
                    break;
                case "wa":
                    wyświetlAutora();
                    break;
                case "ea":
                    edytujAutora();
                    break;
                case "ua":
                    usuńAutora();
                    break;
                case "dk":
                    dodajKsiążkę();
                    break;
                case "wk":
                    wyświetlKsiążkę();
                    break;
                case "ek":
                    edytujKsiążkę();
                    break;
                case "uk":
                    usuńKsiążkę();
                    break;
                case "de":
                    dodajEgzemplarz();
                    break;
                case "ue":
                    usuńEgzemplarz();
                    break;
                case "q":
                    guard = false;
                    break;
                case "a":
                    guard = false;
                    break;
                default:
                    System.out.println("Niewłaściwa opcja. Wpisz h, aby "
                            + "uzyskać pomoc.");
                
            }
        }
    }
    
    private Connection c;
    private Scanner sc;
    private Statement stmt;
    private ResultSet rs;

    private void wyświetlMenu() {
        System.out.println("Wybierz właściwą opcję:");
        System.out.println("dk Dodaj nową książkę");
        System.out.println("wk Wyświetl szczegóły książki");
        System.out.println("ek Edytuj książkę");
        System.out.println("uk Usuń książkę");
        System.out.println("de Dodaj nowy egzamplarz");
        System.out.println("ue Usuń egzemplarz");
        System.out.println("da Dodaj nowego autora");
        System.out.println("wa Wyświetl szczegóły autora");
        System.out.println("ea Edytuj autora");
        System.out.println("ua Usuń autora");
        System.out.println("q Wyjście z panelu administracji książkami");
        System.out.println("Wpisz a w dowolnym momencie, aby anulować "
                + "aktualnie wykonywaną czynność.");
    }
    
    private String czytaj() throws Anulowanie {
        String s = sc.nextLine();
        if ("a".equals(s)) {
            System.out.println("Anulowano operację. Powrót do menu.");
            throw new Anulowanie();
        }
        return s;
    }
    
    private boolean czytajTakNie() throws Anulowanie {
        while (true) {
            String s = czytaj();
            switch (s) {
                case "t":
                    return true;
                case "n":
                    return false;
                default:
                    System.out.println("Wprowadź poprawną wartość.");
            }
        }
    }
    
    private int dodaneId(String tabela) {
        String sql = "SELECT currval(pg_get_serial_sequence('" + tabela 
                + "','id_" + tabela + "')) AS aktualny;";
        int id = 0;
        try {        
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                id = rs.getInt("aktualny");
            }
        } catch (SQLException e) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
        }
        return id;
    }

    private int wczytajAutora() throws Anulowanie {
        System.out.println("Czy znasz identyfikator autora (t/n)?");
        boolean czy = false;
        czy = czytajTakNie();
        
        if (czy) {
            System.out.println("Podaj identyfikator autora:");
            return Integer.parseInt(czytaj());
        } else {
            String nazwisko;

            System.out.println("Podaj nazwisko autora:");
            nazwisko = czytaj();
            
            // wyświetlenia autorów o danym nazwisku
            ArrayList<Integer> identyfikatory = new ArrayList<Integer>();
            int i = 0;
            try {
                PreparedStatement pstmt = c.prepareStatement
                    ("SELECT id_autor, imie, nazwisko FROM autor WHERE "
                    + "nazwisko = ?;");
                pstmt.setString(1, nazwisko);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    System.out.println(++i + ": " + rs.getString("imie") + " " 
                            + rs.getString("nazwisko") + ", identyfikator: "
                            + rs.getInt("id_autor"));
                    identyfikatory.add(rs.getInt("id_autor"));
                }
            } catch (SQLException e) {
                System.err.println( e.getClass().getName()+": "+ e.getMessage() );
                System.exit(0);
            }
            
            if (i == 0) {
                System.out.println("Nie znaleziono odpowiedniego autora.");
                System.out.println("Anuluję operację. Powrót do menu.");
                throw new Anulowanie();
            }
            
            int nr;
            System.out.println("Wybierz numer wpisu z powyższej listy.");
            while (true) {
                String s = czytaj();
                nr = Integer.parseInt(s);
                if (nr <= i && nr > 0)
                    break;
                System.out.println("Wprowadź poprawną wartość");
            }
            
            return identyfikatory.get(nr-1);
        }
        
        
    }
    
    private int wczytajKsiążkę() throws Anulowanie {
        System.out.println("Czy znasz identyfikator książki (t/n)?");
        boolean czy = false;
        czy = czytajTakNie();
        
        if (czy) {
            System.out.println("Podaj identyfikator książki.");
            return Integer.parseInt(czytaj());
        } else {
            String nazwa;
            System.out.println("Podaj nazwę książki:");
            nazwa = czytaj();

            // wyświetlenie książek o danej nazwie
            ArrayList<Integer> identyfikatory = new ArrayList<Integer>();
            int i = 0;
            try {
                PreparedStatement pstmt = c.prepareStatement
                    ("SELECT id_ksiazka, nazwa, imie, nazwisko "
                    + "FROM ksiazka JOIN autor USING (id_autor) "
                    + "WHERE nazwa = ?;");
                pstmt.setString(1, nazwa);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    System.out.println(++i + ": " + rs.getString("nazwa") + ", "
                            + rs.getString("imie") + " " 
                            + rs.getString("nazwisko") + ", identyfikator: "
                            + rs.getInt("id_ksiazka"));
                    identyfikatory.add(rs.getInt("id_ksiazka"));
                }
            } catch (SQLException e) {
                System.err.println( e.getClass().getName()+": "+ e.getMessage() );
                System.exit(0);
            }
            
            if (i == 0) {
                System.out.println("Nie znaleziono odpowiedniej książki.");
                System.out.println("Anuluję operację. Powrót do menu.");
                throw new Anulowanie();
            }
            
            int nr;
            System.out.println("Wybierz numer wpisu z powyższej listy.");
            while (true) {
                String s = czytaj();
                nr = Integer.parseInt(s);
                if (nr <= i && nr > 0)
                    break;
                System.out.println("Wprowadź poprawną wartość");
            }
            
            return identyfikatory.get(nr-1);
        }    
    }
    
    private void dodajKsiążkę() {
        String sql;
        System.out.println("Dodawanie nowej książki.");
        String nazwa = null;
        int isbn = 0, id_autor = 0;
        
        
        System.out.println("Podaj nazwę książki:");
        try {
            nazwa = czytaj();
        } catch (Anulowanie ex) {
            return;
        }
        
        System.out.println("Podaj ISBN:");
        try {
            isbn = Integer.parseInt(czytaj());
        } catch (Anulowanie ex) {
            return;
        }
        
        try {
            id_autor = wczytajAutora();
        } catch (Anulowanie ex) {
            return;
        }
        
        try {
            PreparedStatement pstmt = c.prepareStatement
                    ("INSERT INTO ksiazka(nazwa, isbn, id_autor) VALUES(?, ?, ?);");
            pstmt.setString(1, nazwa);
            pstmt.setInt(2, isbn);
            pstmt.setInt(3, id_autor);
            pstmt.execute();
        } catch ( SQLException e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
        }
        
        System.out.println("Książka dodana pomyślnie. Przydzielony "
                + "identyfikator: " + dodaneId("ksiazka") + ".");
    }

    private void edytujKsiążkę() {
        System.out.println("Edycja książki.");
        int id_książka = 0, isbn = 0, id_autor = 0;
        String nazwa;
        try {
            id_książka = wczytajKsiążkę();
        } catch (Anulowanie ex) {
            return;
        }
    
        System.out.println("Podaj nową nazwę:");
        try {
            nazwa = czytaj();
        } catch (Anulowanie ex) {
            return;
        }
        
        System.out.println("Podaj nowe ISBN:");
        try {
            isbn = Integer.parseInt(czytaj());
        } catch (Anulowanie ex) {
            return;
        }
        
        System.out.println("Podaj nowego autora:");
        try {
            id_autor = wczytajAutora();
        } catch (Anulowanie ex) {
            return;
        }

        try {
            PreparedStatement pstmt = c.prepareStatement
                ("UPDATE ksiazka SET nazwa= ?, isbn = ?, id_autor = ? "
                + "WHERE id_ksiazka = ?;");
            pstmt.setString(1, nazwa);
            pstmt.setInt(2, isbn);
            pstmt.setInt(3, id_autor);
            pstmt.setInt(4, id_książka);
            pstmt.execute();
        } catch ( SQLException e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
        }
        
        System.out.println("Edycja książki zakończona powodzeniem.");
    }

    private void usuńKsiążkę(int id_książka) {
        
        
        String sql = "DELETE FROM ksiazka WHERE id_ksiazka="+ id_książka + ";";
        try {    
            stmt.executeUpdate(sql);
        } catch ( SQLException e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
        }
        
    }
    
    private void usuńKsiążkę() {
        System.out.println("Usuwanie książki.");
        int id_książka = 0;
        try {
            id_książka = wczytajKsiążkę();
        } catch (Anulowanie ex) {
            return;
        }
        
        usuńKsiążkę(id_książka);
        
        System.out.println("Usunięto książkę wraz ze wszystkimi odwołaniami.");
    }

    private void dodajEgzemplarz() {
        String sql;
        System.out.println("Dodawanie nowego egzemplarza.");
        int id_książka = 0;
        
        try {
            id_książka = wczytajKsiążkę();
        } catch (Anulowanie ex) {
            return;
        }
        
        sql = "INSERT INTO egzemplarz(id_ksiazka) VALUES(" + id_książka + ");";
        
        try {     
            stmt.executeUpdate(sql);
        } catch ( SQLException e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
        }
        
        System.out.println("Egzemplarz dodano pomyślnie. Przydzielony "
                + "identyfikator: " + dodaneId("egzemplarz") + ".");
    }

    private void usuńEgzemplarz(int id_egzemplarz) {
        
        String sql = "DELETE FROM egzemplarz WHERE id_egzemplarz="+ id_egzemplarz + ";";
        try {    
            stmt.executeUpdate(sql);
        } catch ( SQLException e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
        }
        
    }
    
    private void usuńEgzemplarz() {
        System.out.println("Usuwanie egzemplarza.");
        System.out.println("Podaj identyfikator egzemplarza:");
        int id_egzemplarz = 0;
        try {
            id_egzemplarz = Integer.parseInt(czytaj());
        } catch (Anulowanie ex) {
            return;
        }
        
        usuńEgzemplarz(id_egzemplarz);
        
        System.out.println("Usunięto egzemplarz wraz ze wszystkimi odwołaniami.");
    }
    
    private void dodajAutora() {
        System.out.println("Dodawanie nowego autora.");
        String imię = null, nazwisko = null;
        
        System.out.println("Podaj imię:");
        try {
            imię = czytaj();
        } catch (Anulowanie ex) {
            return;
        }
        
        System.out.println("Podaj nazwisko:");
        try {
            nazwisko = czytaj();
        } catch (Anulowanie ex) {
            return;
        }
        
        try {
            PreparedStatement pstmt = c.prepareStatement
                ("INSERT INTO autor(imie, nazwisko) VALUES(?, ?);");
            pstmt.setString(1, imię);
            pstmt.setString(2, nazwisko);
            pstmt.execute();
        } catch ( SQLException e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
        }

        System.out.println("Autor dodany pomyślnie. Przydzielony "
                + "identyfikator: " + dodaneId("autor") + ".");
    }
    
    private void edytujAutora() {
        System.out.println("Edycja autora.");
        int id_autor = 0;
        String imię, nazwisko;
        try {
            id_autor = wczytajAutora();
        } catch (Anulowanie ex) {
            return;
        }
    
        System.out.println("Podaj nowe imię:");
        try {
            imię = czytaj();
        } catch (Anulowanie ex) {
            return;
        }
        
        System.out.println("Podaj nowe nazwisko:");
        try {
            nazwisko = czytaj();
        } catch (Anulowanie ex) {
            return;
        }
        
        try {
            PreparedStatement pstmt = c.prepareStatement
                ("UPDATE autor SET imie = ?, nazwisko = ? WHERE id_autor = ?;");
            pstmt.setString(1, imię);
            pstmt.setString(2, nazwisko);
            pstmt.setInt(3, id_autor);
            pstmt.execute();
        } catch ( SQLException e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
        }
        
        System.out.println("Edycja autora zakończona powodzeniem.");
    
    }

    private void usuńAutora(int id_autor) {

        String sql = "DELETE FROM autor WHERE id_autor="+ id_autor + ";";
        try {    
            stmt.executeUpdate(sql);
        } catch ( SQLException e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
        }
    }

    private void usuńAutora() {
        System.out.println("Usuwanie autora.");
        int id_autor = 0;
        try {
            id_autor = wczytajAutora();
        } catch (Anulowanie ex) {
            return;
        }
        
        usuńAutora(id_autor);
        
        System.out.println("Usunięto autora wraz ze wszystkimi odwołaniami.");
    }

    private void wyświetlAutora() {
        System.out.println("Wyświetlanie szczegółów autora.");
        int id_autor = 0;
        try {
            id_autor = wczytajAutora();
        } catch (Anulowanie ex) {
            return;
        }
        
        int ile = 0;
        String sql = "SELECT imie, nazwisko, liczba_ksiazek "
                    + "FROM autor WHERE id_autor = " + id_autor + ";";
        try {        
            rs = stmt.executeQuery(sql);
            rs.next();
            ile = rs.getInt("liczba_ksiazek");
            System.out.println("Autor: "
                    + rs.getString("imie") + " " 
                    + rs.getString("nazwisko") + ", identyfikator: "
                    + id_autor + ".");
            System.out.println("Napisanych książek: " 
                    + ile + ".");
        } catch (SQLException e) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
        }
        
        if (ile == 0)
            return;
        
        System.out.println("Czy chcesz wyświetlić listę książek (t/n)?");
        boolean czy = false;
        try {
            czy = czytajTakNie();
        } catch (Anulowanie ex) {
            return;
        }
        
        if (czy) {
            System.out.println("Napisane książki:");

            sql = "SELECT nazwa FROM ksiazka "
                    + "WHERE id_autor = " + id_autor + ";";
            try {     
                rs = stmt.executeQuery(sql);
                boolean czy_coś = false;
                while (rs.next()) {
                    czy_coś = true;
                    System.out.println("- \"" + rs.getString("nazwa") + "\"");
                }
                if (!czy_coś)
                    System.out.println("Brak napisanych książek");
            } catch ( SQLException e ) {
                System.err.println( e.getClass().getName()+": "+ e.getMessage() );
                System.exit(0);
            }
        }
    
    }

    private void wyświetlKsiążkę() {
        System.out.println("Wyświetlanie szczegółów książki.");
        int id_książka = 0;
        try {
            id_książka = wczytajKsiążkę();
        } catch (Anulowanie ex) {
            return;
        }
        
        String sql = "SELECT nazwa, ISBN, imie, nazwisko "
                + "FROM ksiazka JOIN autor USING (id_autor) "
                + "WHERE  id_ksiazka = " + id_książka + ";";
        try {        
            rs = stmt.executeQuery(sql);
            rs.next();
            System.out.println("Książka: \"" + rs.getString("nazwa") 
                    + "\", autor: " 
                    + rs.getString("imie") + " " 
                    + rs.getString("nazwisko")
                    + ", ISBN: " + rs.getInt("isbn") + ", identyfikator: "
                    + id_książka + ".");
        } catch (SQLException e) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
        }
        
        int ile = 0;
        sql = "SELECT COUNT('x') AS ile "
                + "FROM ksiazka JOIN egzemplarz USING (id_ksiazka) "
                + "WHERE id_ksiazka = " + id_książka + ";";
        try {     
            rs = stmt.executeQuery(sql);
            rs.next();
            ile = rs.getInt("ile");
            System.out.println("Liczba egzemplarzy: " + ile + ".");

        } catch ( SQLException e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
        }
        
        if (ile == 0)
                return;
        
        System.out.println("Czy chcesz wyświetlić zużycie egzemplarzy (t/n)?");
        boolean czy = false;
        try {
            czy = czytajTakNie();
        } catch (Anulowanie ex) {
            return;
        }
        
        if (czy) {
            System.out.print("Kolejne egzemplarze były wypożyczone przez następującą liczbę dni: ");
            sql = "SELECT dni_wypozyczone "
                    + "FROM ksiazka JOIN egzemplarz USING (id_ksiazka) "
                    + "WHERE id_ksiazka = " + id_książka
                    + "ORDER BY dni_wypozyczone DESC;";
            try {
                rs = stmt.executeQuery(sql);
                boolean czy_coś = false;
                while(rs.next()) {
                    if (czy_coś)
                        System.out.print(", ");
                    czy_coś = true;
                    System.out.print(rs.getInt("dni_wypozyczone"));
                }
                System.out.println(".");
            } catch ( SQLException e ) {
                System.err.println( e.getClass().getName()+": "+ e.getMessage() );
                System.exit(0);
            }
        }
    }

}
