/** Projekt biblioteka
 * Aleksander Matusiak
 * 
 * Klasa główna
 */

package biblioteka;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 *
 * @author aleksander
 */
public class Biblioteka {
    

    private static void connect() {
        c = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/bd",
              "login", "password");
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(0);
        }
    }

    public static void main(String[] args) {
        connect();
        
        Pracownik p = new Pracownik(c);
        p.obsługa();
        p.close();
    }
    
    private static Connection c;

}
