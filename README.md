#Library management#
## Project created as an assignment for *Databases* course##

### Overview  ###
This program is designed for storing information about books, books' copies in the library and authors. It also enables to store data about readers, register all loans, returns and extensions of a return time. 

### Functionalities ###

####Managing readers:####

* adding/editing/deleting a reader

* displaying currently lent books

* lending/returning a book

* extending a return time of the book


####Managing books:####

* adding/editing/deleting a book

* displaying information about the book

* adding/editing/deleting an author

* displaying information about the author

* adding/deleting a book copy