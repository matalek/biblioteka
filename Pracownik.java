/** Projekt biblioteka
 * Aleksander Matusiak
 * 
 * Klasa do obsługi codziennych czynności biblioteki
 */

package biblioteka;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aleksander
 */
public class Pracownik {

    public Pracownik(Connection c) {
        this.c = c;
        try {
            stmt = c.createStatement();
        } catch (SQLException ex) {
            Logger.getLogger(Pracownik.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void close() {
        try {
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(Pracownik.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void obsługa() {
        sc = new Scanner(System.in);
        boolean guard = true;
        System.out.println("Wpisz h, aby uzyskać pomoc. Wpisz a w dowolnym "
                + "momencie, aby anulować aktualnie wykonywaną czynność.");
        while (guard) {
            switch (sc.nextLine()) {
                case "h":
                    wyświetlMenu();
                    break;
                case "w":
                    wypożycz();
                    break;
                case "z":
                    zwróć();
                    break;
                case "p":
                    prolonguj();
                    break;
                case "c":
                    czytelnik();
                    break;
                case "e":
                    edytujCzytelnika();
                    break;
                case "d":
                    dodajCzytelnika();
                    break;
                case "k":
                    administrujKsiążkami();
                    break;
                case "q":
                    guard = false;
                    break;
                case "a":
                    break;
                default:
                    System.out.println("Niewłaściwa opcja. Wpisz h, aby "
                            + "uzyskać pomoc.");
            }
        }
    }
    
    public static int MAX_PROLONGAT = 3;
    public static int CZAS_WYPOŻYCZENIA = 30;
    
    private Connection c;
    private Scanner sc;
    private Statement stmt;
    private PreparedStatement pstmt;
    private ResultSet rs;
    private static long MILLISECONDS_IN_DAY = 24 * 60 * 60 * 1000;
    
    
    private void wyświetlMenu() {
        System.out.println("Wybierz właściwą opcję:");
        System.out.println("w Wypożycz książkę");
        System.out.println("z Zwróć książkę");
        System.out.println("p Prolonguj książkę");
        System.out.println("c Wyświetl czytelnika");
        System.out.println("e Edytuj dane czytelnika");
        System.out.println("d Dodaj nowego czytelnika");
        System.out.println("k Włącz panel administracji książkami");
        System.out.println("q Wyjście");
        System.out.println("Wpisz a w dowolnym momencie, aby anulować "
                + "aktualnie wykonywaną czynność.");
    }
    
    // wczytuje linię z uwzględnieniem możliwości anulowania
    private String czytaj() throws Anulowanie {
        String s = sc.nextLine();
        if ("a".equals(s)) {
            System.out.println("Anulowano operację. Powrót do menu.");
            throw new Anulowanie();
        }
        return s;
    }
    
    // zwraca id przed chwilą dodanego wpisu w tabeli
    private int dodaneId(String tabela) {
        String sql = "SELECT currval(pg_get_serial_sequence('" + tabela 
                + "','id_" + tabela + "')) AS aktualny;";
        int id = 0;
        try {        
            rs = stmt.executeQuery(sql);
            rs.next();
            id = rs.getInt("aktualny");
        } catch (SQLException e) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
        }
        return id;
    }
    
    private int wczytajCzytelnika() throws Anulowanie {
        String sql;
        
        // pobranie identyfikatora czytelnika i wyświetlenie informacji o nim
        System.out.println("Podaj identyfikator czytelnika:");
        int id_czytelnik = Integer.parseInt(czytaj());
        sql = "SELECT imie, nazwisko FROM czytelnik WHERE id_czytelnik = " +
            id_czytelnik + ";";
        try {        
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                String imie = rs.getString("imie");
                String nazwisko = rs.getString("nazwisko");
                System.out.println("Czytelnik: " + imie + " " + nazwisko);
            } else {
                System.out.println("Nie ma takiego czytelnika.");
                System.out.println("Anuluję operację. Powrót do menu.");
                throw new Anulowanie();
            }
        } catch (SQLException e) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
        }
        return id_czytelnik;
    }
    
    private int wczytajEgzemplarz() throws Anulowanie {
        String sql;
        
        // pobranie identyfikatora egzemplarza i wyświetlenie informacji
        // o książce
        System.out.println("Podaj identyfikator egzemplarza książki:");
        int id_egzemplarz = Integer.parseInt(czytaj());
        sql = "SELECT nazwa, imie, nazwisko "
                + "FROM egzemplarz JOIN ksiazka USING (id_ksiazka) "
                + "JOIN autor USING (id_autor)"
                + "WHERE id_egzemplarz = " + id_egzemplarz + ";";
        try {      
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                String nazwa = rs.getString("nazwa");
                String imie = rs.getString("imie");
                String nazwisko = rs.getString("nazwisko");
                System.out.println("Książka \"" + nazwa + "\" autorstwa " + imie + " " + nazwisko);
            } else {
                System.out.println("Nie ma takiego egzemplarza.");
                System.out.println("Anuluję operację. Powrót do menu.");
                throw new Anulowanie();
            }

        } catch ( SQLException e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
        }
        
        return id_egzemplarz;
    }
    
    private void wypożycz() {
        String sql;
        String s;
        
        System.out.println("Wypożyczanie książki.");
        
        int id_czytelnik;
        int id_egzemplarz;
        
        try {
            id_czytelnik = wczytajCzytelnika();
            id_egzemplarz = wczytajEgzemplarz();
        } catch (Anulowanie e) {
            return;
        }
        
        // dokonanie wypożyczenia
        Calendar cal = Calendar.getInstance();
        java.util.Date data = cal.getTime();
        java.sql.Date data_wypożyczenia = new java.sql.Date(data.getTime());
        cal.add(Calendar.DATE, CZAS_WYPOŻYCZENIA);
        data = cal.getTime();
        java.sql.Date termin_zwrotu = new java.sql.Date(data.getTime());
        
        sql = "INSERT INTO wypozyczenie VALUES(" + id_egzemplarz + ","
                + id_czytelnik + ",'" + data_wypożyczenia + "','" 
                + termin_zwrotu + "',0);";
        
        try {     
            stmt.executeUpdate(sql);
        } catch ( SQLException e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
        }
        
        System.out.println("Książka wypożyczona do: " + termin_zwrotu);
    }

    private void zwróć() {
        String sql;
        String s;
        
        System.out.println("Zwrot książki.");
        
        int id_egzemplarz;
        try {
            id_egzemplarz = wczytajEgzemplarz();
        } catch (Anulowanie e) {
            return;
        }
        
        // wyliczenie, ile przetrzymano książkę oraz wyświetlnie, kto wypożyczył
        sql = "SELECT termin_zwrotu, imie, nazwisko "
                + "FROM wypozyczenie JOIN czytelnik USING (id_czytelnik)"
                + "WHERE id_egzemplarz = " + id_egzemplarz + ";";
        java.sql.Date termin_zwrotu = null;
        try {       
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                System.out.println("Wypożyczone przez: " + rs.getString("imie")
                    + " " + rs.getString("nazwisko") + ".");
                termin_zwrotu = rs.getDate("termin_zwrotu");
            } else {
                System.out.println("Ten egzemplarz nie był wypożyczony.");
                System.out.println("Anuluję operację. Powrót do menu.");
                return;
            }
        } catch ( SQLException e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
        }
        
        Calendar cal = Calendar.getInstance();
        java.util.Date data = cal.getTime();
        int ile_przetrzymano = (int)((data.getTime() - termin_zwrotu.getTime())/MILLISECONDS_IN_DAY);
        if (ile_przetrzymano > 0)
            System.out.println("Książkę przetrzymano o " + ile_przetrzymano + " dni.");
        else
            System.out.println("Nie przekroczono terminu zwrotu.");
        
        // usuwanie wpisu
        sql = "DELETE FROM wypozyczenie WHERE id_egzemplarz="+ id_egzemplarz + ";";
        
        try {    
            stmt.executeUpdate(sql);
        } catch ( SQLException e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
        }
        
        System.out.println("Książka pomyślnie zwrócona.");
        
        
    }

    private void prolonguj() {
        String sql;
        String s;
        
        System.out.println("Prolongowanie terminu zwrotu książki.");

        int id_egzemplarz;
        try {
            id_egzemplarz = wczytajEgzemplarz();
        } catch (Anulowanie e) {
            return;
        }
        
        // pobranie akutalnego terminu zwrotu oraz wyświetlenie, kto wypożyczył
        sql = "SELECT termin_zwrotu, liczba_prolongat, imie, nazwisko "
                + "FROM wypozyczenie JOIN czytelnik USING (id_czytelnik)"
                + "WHERE id_egzemplarz = " + id_egzemplarz + ";";
        java.sql.Date termin_zwrotu = null;
        int liczba_prolongat = 0;
        try {       
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                System.out.println("Wypożyczone przez: " + rs.getString("imie")
                    + " " + rs.getString("nazwisko") + ".");
                termin_zwrotu = rs.getDate("termin_zwrotu");
                liczba_prolongat = rs.getInt("liczba_prolongat");
            } else {
                System.out.println("Ten egzemplarz nie był wypożyczony.");
                System.out.println("Anuluję operację. Powrót do menu.");
                return;
            }
        } catch ( SQLException e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
        }
        
        System.out.println("Aktualny termin zwrotu: " + termin_zwrotu);
        System.out.println("Do tej pory prolongowano " + liczba_prolongat + " razy.");
        if (liczba_prolongat == MAX_PROLONGAT) {
            System.out.println("Nie można więcej razy prolongować terminu.");
            return;
        }
        System.out.println("O ile dni prolongować termin zwrotu?");
        try {
            s = czytaj();
        } catch (Anulowanie e) {
            return;
        }
        int o_ile = Integer.parseInt(s);
        
        Calendar cal = Calendar.getInstance();
        java.util.Date data = new java.util.Date(termin_zwrotu.getTime());
        cal.setTime(data);
        cal.add(Calendar.DATE, o_ile);
        data = cal.getTime();
        java.sql.Date nowy_termin = new java.sql.Date(data.getTime());
        
        System.out.println("Nowy termin zwrotu: " + nowy_termin);
        
        // uaktualnianie wpisu
        liczba_prolongat++;
        sql = "UPDATE wypozyczenie SET termin_zwrotu='"+ nowy_termin
                + "', liczba_prolongat = "+ liczba_prolongat 
                + " WHERE id_egzemplarz="+ id_egzemplarz + ";";
        
        try {      
            stmt.executeUpdate(sql);
        } catch ( SQLException e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
        }
        
        System.out.println("Termin zwrotu pomyślnie prolongowany.");
    }

    private void czytelnik() {
        int id_czytelnik;
        try {
            id_czytelnik = wczytajCzytelnika();
        } catch (Anulowanie e) {
            return;
        }
        
        System.out.println("Wypożyczone książki:");
        
        String sql;
        
        // wyświetlenie wypożyczonych książek
        sql = "SELECT data_wypozyczenia, termin_zwrotu, liczba_prolongat, "
                + "nazwa, autor.imie, autor.nazwisko "
                + "FROM czytelnik JOIN wypozyczenie USING (id_czytelnik) "
                + "JOIN egzemplarz USING (id_egzemplarz) "
                + "JOIN ksiazka USING (id_ksiazka) "
                + "JOIN autor USING (id_autor) "
                + "WHERE id_czytelnik = " + id_czytelnik + ";";
        try {     
            rs = stmt.executeQuery(sql);
            boolean czy_coś = false;
            while (rs.next()) {
                czy_coś = true;
                java.sql.Date data_wypożyczenia = rs.getDate("data_wypozyczenia");
                java.sql.Date termin_zwrotu = rs.getDate("termin_zwrotu");
                int liczba_prolongat = rs.getInt("liczba_prolongat");
                String nazwa = rs.getString("nazwa");
                String imie = rs.getString("imie");
                String nazwisko = rs.getString("nazwisko");
                System.out.println("Książka: \"" + nazwa + "\" autorstwa " + imie 
                    + " " + nazwisko +", wypożyczona " + data_wypożyczenia
                    + " do " + termin_zwrotu + ", prolongowana "
                    + liczba_prolongat + " raz(y).");
            }
            if (!czy_coś)
                System.out.println("Brak wypożyczonych książek");
        } catch ( SQLException e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
        }
    }

    private void edytujCzytelnika() {
        
        System.out.println("Edycja czytelnika.");
        String imię = null, nazwisko = null;
        int id_czytelnik;
        
        try {
            id_czytelnik = wczytajCzytelnika();
        } catch (Anulowanie ex) {
            return;
        }
        
        System.out.println("Podaj nowe imię:");
        try {
            imię = czytaj();
        } catch (Anulowanie ex) {
            return;
        }
        
        System.out.println("Podaj nowe nazwisko:");
        try {
            nazwisko = czytaj();
        } catch (Anulowanie ex) {
            return;
        }
        
        try {
            PreparedStatement pstmt = c.prepareStatement("UPDATE czytelnik SET "
                    + "imie= ?, nazwisko = ? WHERE id_czytelnik = ?;");
            pstmt.setString(1, imię);
            pstmt.setString(2, nazwisko);
            pstmt.setInt(3, id_czytelnik);
            pstmt.execute();
        } catch ( SQLException e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
        }
        
        System.out.println("Edycja czytelnika zakończona powodzeniem.");
    }

    private void dodajCzytelnika() {
        String sql;
        
        System.out.println("Dodawanie nowego czytelnika.");
        String imię = null, nazwisko = null;
        
        System.out.println("Podaj imię:");
        try {
            imię = czytaj();
        } catch (Anulowanie ex) {
            return;
        }
        
        System.out.println("Podaj nazwisko:");
        try {
            nazwisko = czytaj();
        } catch (Anulowanie ex) {
            return;
        }
        
        try {
            PreparedStatement pstmt = c.prepareStatement
                ("INSERT INTO czytelnik(imie, nazwisko) VALUES(?, ?);");
            pstmt.setString(1, imię);
            pstmt.setString(2, nazwisko);
            pstmt.execute();
        } catch ( SQLException e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
        }
        
        System.out.println("Czytelnik dodany pomyślnie. Przydzielony "
                + "identyfikator: " + dodaneId("czytelnik") + ".");
    
    }

    private void administrujKsiążkami() {
        AdministracjaKsiążkami ak = new AdministracjaKsiążkami(c, sc, stmt, rs);
        ak.obsługa();
        System.out.println("Opuściłeś panel administracji książkami");
    
    }
}